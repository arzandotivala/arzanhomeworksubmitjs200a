// If an li element is clicked, toggle the class "done" on the <li>

const $li = $('li'); // Grabbing all li elements in the document
$li.on('click', function(e){
  $(this).toggleClass('done');
});

// If a delete link is clicked, delete the li element / remove from the DOM

const $delete = $('a.delete'); //grabbing all delete elements
$delete.on('click', function(e){
  $(this).parent().fadeOut(1000),function(e) {
    $(this).remove(); //https://www.youtube.com/watch?v=7ET7Zl6T29o
  }
  });

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const $move = $('a.move');
const $todayL = $('ul.today-list');
const $laterL = $('ul.later-list');

$move.on('click', function(e){

if ($(this).hasClass('toToday')) { //if the class clicked has toToday than delete the parent node (the to do item) and append
  $todayL.append($(this).parent());
  $(this).text('Move to Later');
  $(this).removeClass('toToday')
  $(this).addClass('toLater'); //add the class to Later to be able to switch back (how to do this?)//add the class to Later to be able to switch back (how to do this?)

}
else if ($(this).hasClass('toLater'))  { //if the class clicked has toLater than delete the parent node (the to do item) and append
  $laterL.append($(this).parent());
  $(this).text('Move to Today');
  $(this).removeClass('toLater')
  $(this).addClass('toToday');
};
});

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)

const addListItem = function(e) {
  e.preventDefault();
  const text = $(this).parent().find('input').val();
 
  const $li = $('<li>')

  const $moveA = $('<a>');

    if ($(this).parent().parent().hasClass('today')) {
    
      $li.html(`<span>${text}</span> <a class="move toToday">Move to Today</a> <a class="delete">Delete</a>`)
      $li.append($moveA);

      $moveA.click(function(e){

        if ($(this).hasClass('toToday')) { //if the class clicked has toToday than delete the parent node (the to do item) and append
          $todayL.append($(this).parent());
          $(this).text('Move to Later');
          $(this).removeClass('toToday')
          $(this).addClass('toLater'); //add the class to Later to be able to switch back (how to do this?)//add the class to Later to be able to switch back (how to do this?)
        
        }
        else if ($(this).hasClass('toLater'))  { //if the class clicked has toLater than delete the parent node (the to do item) and append
          $laterL.append($(this).parent());
          $(this).text('Move to Today');
          $(this).removeClass('toLater')
          $(this).addClass('toToday');
        };
        })

      $later = $('ul.later-list').append($li)
  } 

  else if ($(this).parent().parent().hasClass('later')) {
  $("later-list").html(`<span>${text}</span> <a class="move toLater">Move to Later</a> <a class="delete">Delete</a>`)
  }
};

const $add = $('.add-item');
$add.on('click',addListItem)

//NOTES FROM CLASS 7: $('ul').first().append($li); 
