const handleliclick = function(e){
    if(e.target.classList[0] ==='delete') {
        const elementToRemove = e.target.parentNode;
        elementToRemove.parentNode.removeChild(elementToRemove);
    }
    if( e.target.tagName === 'LI'){
        e.target.classList.toggle('done');
    }

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>

if(e.target.classList.contains("toLater")) { // Today to Later
    const moveToLaterRemove = e.target.parentNode; //step 1 remove the task from Today
    moveToLaterRemove.parentNode.removeChild(moveToLaterRemove);
    
    let later = document.getElementsByClassName("later-list")[0]; //step 2 take the task and append "later"
    e.target.classList.remove("toLater");
    e.target.classList.add("toToday");
    later.appendChild(moveToLaterRemove);
    
    
  let newToday = document.getElementsByClassName("toLater")[0]; //step 3 change "later" to "today"
  newToday.innerHTML = "Move to Today";
}

else if(e.target.classList.contains("toToday")) { //Later to Today
    const moveToLaterRemove = e.target.parentNode; //step 1 remove the task from Later
    moveToLaterRemove.parentNode.removeChild(moveToLaterRemove);
    let later = document.getElementsByClassName("today-list")[0]; //step 2 take the task and append "today"
    e.target.classList.remove("toToday");
    e.target.classList.add("toLater");
    later.appendChild(moveToLaterRemove);
    
    
  let newToday = document.getElementsByClassName("toLater")[0]; //step 3 change "today" to "later"
  newToday.innerHTML = "Move to Later";
}
};

/* <li>
 <span>Homework</span>
 <a class="move toToday">Move to Today</a>
 <a class="delete">Delete</a>
</li> */

// If an li element is clicked, toggle the class "done" on the <li>
// If a delete link is clicked, delete the li element / remove from the DOM

const lielements = document.querySelectorAll('li');
for(i = 0;i<lielements.length; i++) { //classList.contains https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
    lielements[i].addEventListener('click', handleliclick);
}

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
    e.preventDefault();
    const input = this.parentNode.getElementsByTagName('input')[0];
    const text = input.value; // use this text to create a new <li>
    input.innerHTML = ("text");


    if(this.parentNode.parentNode.classList.contains("Today")) { //Step 1 listening to today
    const li = document.createElement('li');
    li.innerHTML =  `<span>${text}</span> <a class="move toLater">Move to Later</a> <a class="delete">Delete</a>`;
    let today = document.getElementsByClassName("today-list")[0];
    today.appendChild(li);
    li.addEventListener('click',handleliclick);
}

    else if (this.parentNode.parentNode.classList.contains("Later")) { //Step 2 listening to Later
    const li = document.createElement('li');
    li.innerHTML =  `<span>${text}</span> <a class="move toToday">Move to Today</a> <a class="delete">Delete</a>`;
    let later = document.getElementsByClassName("later-list")[0];
    later.appendChild(li);
    li.addEventListener('click',handleliclick);
    }
}


  // Add this as a listener to the two Add links
  const addToday = document.getElementsByClassName("add-item")[0];
  addToday.addEventListener('click',addListItem);

  const addLater = document.getElementsByClassName("add-item")[1];
  addLater.addEventListener('click',addListItem);

 
